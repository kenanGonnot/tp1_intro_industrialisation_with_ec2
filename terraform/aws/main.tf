# PROVIDER
provider "aws" {
  region = var.region
}

resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = var.aws_public_key_ssh_path
}

#terraform import aws_key_pair.deployer deployer-key


resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

#terraform import aws_default_vpc.default vpc-a01106c2

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_default_security_group" "default_sg" {
  vpc_id = aws_vpc.mainvpc.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


#terraform import aws_default_security_group.default_sg sg-903004f8

resource "aws_instance" "my-ec2" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = "admin"

  tags = {
    Name = var.tag_name
  }
  depends_on = [aws_key_pair.admin]
}


#terraform import aws_instance.web i-12345678